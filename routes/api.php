<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Auth\LoginController@login');
Route::post('home', 'HomeController@index');
Route::post('product', 'ProductController@index');
Route::post('bid', 'ProductController@bid');
Route::post('maxBidAmount', 'ProductController@maxBidAmount');
//Route::post('login', 'SimpleLogin@login');
