<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'name' => 'Antique Decor Clock with Compass',
                'description' => 'Antique Nautical Victorian London Brass Table Top Decor Clock with Compass',
                'price' => '10',
                'close_date_time' =>'2020-12-31 00:00:00',
            ],
            [
                'name' => 'Brass Balance Scale',
                'description' => 'Antique Gold and Diamond Weighing Scales - Brass Balance Scale - Made in India Brass Gold Scales With Wooden Base Scales pocket sized',
                'price' => '15',
                'close_date_time' =>'2021-01-30 00:00:00',
            ],
            [
                'name' => 'Antique box',
                'description' => 'Antique box sewing items',
                'price' => '20',
                'close_date_time' =>'2021-03-30 00:00:00',
            ],
            [
                'name' => 'Vintage Art Antique Brass',
                'description' => 'Vintage Art Antique Brass - The Cox London Thread Sundial Compass Antique Home Décor Table Item, Ideal Gifts For him',
                'price' => '25',
                'close_date_time' =>'2021-01-30 00:00:00',
            ],
            [
                'name' => 'Wooden Car',
                'description' => 'Handicraft Vintage Wooden Car Decorative Showpiece',
                'price' => '30',
                'close_date_time' =>'2021-02-30 00:00:00',
            ],
            [
                'name' => 'Dinner Set',
                'description' => 'Very Rare Antique MINTON PINK COCKATRICE Dinner Set items, Individually Sold Superb Quality China',
                'price' => '35',
                'close_date_time' =>'2020-12-31 00:00:00',
            ],
            [
                'name' => 'Gramaphone',
                'description' => 'Vintage looking Antique Type Gramaphone Gift Item',
                'price' => '40',
                'close_date_time' =>'2021-01-30 00:00:00',
            ],
            [
                'name' => 'Silver Choker',
                'description' => 'Choker, low sterling silver, 24 coins, antique item, vintage item, throat pin',
                'price' => '45',
                'close_date_time' =>'2021-01-30 00:00:00',
            ],
            [
                'name' => 'Antique Ring',
                'description' => 'Amazing Peridot Gemstone Ring,Boho Ring 925-Sterling Silver Ring,Antique Silver Ring Small Stone Ring,Ring Finger Ring Gift Item Ring',
                'price' => '50',
                'close_date_time' =>'2021-01-30 00:00:00',
            ],
            [
                'name' => 'Natural Rainbow Moonstone',
                'description' => 'Natural Rainbow Moonstone Top Quality Gemstone Necklace,Pear Cabochon Necklace 925-Sterling Solid Silver Necklace For Girls Gift For Her !!',
                'price' => '55',
                'close_date_time' =>'2021-01-30 00:00:00',
            ],
            [
                'name' => 'Fm Radio',
                'description' => 'Antique Type Vintage Looking Fm Radio for gift Item',
                'price' => '60',
                'close_date_time' =>'2021-01-30 00:00:00',
            ],
     
        ]);
    }
}
