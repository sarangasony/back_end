<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
               'username' => 'user1',
               'password' => Hash::make('user1'),
               'api_token' => '1234',
            ],
            [
                'username' => 'user2',
                'password' => Hash::make('user2'),
                'api_token' => '2345',
            ],
        ]);
    }
}
