<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $query = \App\Product::query();
        if (request()->queryString) {
            $query = $query->where('name', 'like', '%' . request()->queryString . '%')
            ->orWhere('description', 'like', '%' . request()->queryString . '%');
        }
        $query = $query->orderBy('price',request()->priceOrder);

        return response()->json(

            $query->paginate(5)
          //DB::table($table)->paginate($perPage,['*'],'page',$page); 
        );
    }
}
