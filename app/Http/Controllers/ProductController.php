<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $model = \App\Product::find(request()->product_id);
        return response()->json(
            ['product'=>$model, 'bids'=>$model->bids]
        );
    }


    public function bid()
    {
        
        $latestBid = \App\Bid::where('product_id', request()->product_id )->orderBy('id', 'desc')->first();
        $success = false;
        

        $price = 0;
        $minPrice = 0;
        if($latestBid) {
            $minPrice = $latestBid->bid_price;
            if( $latestBid->bid_price < request()->price)
                $price  = request()->price;
        } else {
            $model = \App\Product::find(request()->product_id);
            $minPrice = $model->price;
            if($model->price < request()->price) {
                $price = request()->price;
            }
        }

        $msg = 'Amount should be grather than '.$minPrice;

        if($price) {
            $bid = new \App\Bid;
            $bid->user_id = request()->user_id ;
            $bid->product_id = request()->product_id;
            $bid->auto_bidding = request()->auto_bidding? 1: 0;
            $bid->bid_price = $price;
            $bid->save();
            $s = $bid->autoBidding();
            $success = true;
            $msg = 'Saved successfully...';
        } 

        return response()->json(
            [ 'success' => $success, 'message' => $msg, 's'=> $s ]
        );
    }


    public function maxBidAmount() 
    {
        try {
            $user = \App\User::find(request()->user_id);
            $user->maximum_bid_amount = request()->maximum_bid_amount ;
            $user->save();        
            return response()->json([
                'success'=>true ,'message' => 'Saved successfully...'
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'success'=>false , 'message' => 'Something wrong...',
            ], 200);
        }

    }
}
