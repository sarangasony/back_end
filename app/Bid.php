<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    //
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function autoBidding()
    {
        // Get all bids by product 
        $bids = Bid::getBidsByProduct($this->product_id);

        $autobidActivetedUser = $users = [];

        foreach($bids as $bid) {
            $users[$bid->user_id] = $bid->auto_bidding;
        }

        // get product with autobid activated users
        foreach($users as $userId => $autobid) {
            if($autobid) {
                $autobidActivetedUser[$userId] = self::getAutobids($userId);
            }
        }

       

        if($autobidActivetedUser) {

            $maxBidPriceForProduct = [];
            $maxBidUserId = 0;
            if(count($autobidActivetedUser) > 1) {
                foreach($autobidActivetedUser as $userId => $products) {
                    $user = User::find($userId);
                    $maxBidPriceForProduct[$userId] = round($user->maximum_bid_amount / count($products) );
                }

                asort($maxBidPriceForProduct);
                $maxBidUserPrice = end($maxBidPriceForProduct);
                $maxBidUserId = key($maxBidPriceForProduct);
                $maxBidPrice = prev($maxBidPriceForProduct);

                if($maxBidUserPrice > $maxBidPrice) {
                    $maxBidPriceForProduct[$maxBidUserId] = $maxBidPrice +1;
                }

                foreach($maxBidPriceForProduct as $userId => $price) {

                    $bid = new Bid;
                    $bid->user_id = $userId;
                    $bid->product_id = $this->product_id;
                    $bid->auto_bidding = $maxBidUserId == $userId  ? 1: 0;
                    $bid->bid_price = $price;
                    $bid->save();

                }
            } else {

            //return  $maxBidPriceForProduct;


                foreach($autobidActivetedUser as $userId => $products) {

                    $user = User::find($userId);

                    // if(is_array($maxBidPriceForProduct) && $maxBidPriceForProduct ) {

                    //     $bid = new Bid;
                    //     $bid->user_id = $userId;
                    //     $bid->product_id = $this->product_id;
                    //     $bid->auto_bidding = $maxBidUserId == $userId  ? 1: 0;
                    //     $bid->bid_price = $maxBidPriceForProduct[$userId];
                    //     $bid->save();

                    // } else {
                        $maxBid = $user->maximum_bid_amount / count($products);

                        $latestBid = Bid::where('product_id', $this->product_id )->orderBy('id', 'desc')->first();


                        if($latestBid->user_id != $userId) {
                            
                            if( $maxBid >= $latestBid->bid_price + 1 ) {
                                $bid = new Bid;
                                $bid->user_id = $userId;
                                $bid->product_id = $this->product_id;
                                $bid->auto_bidding = 1;
                                $bid->bid_price = $latestBid->bid_price + 1;
                                $bid->save();
                            }
                        }

                    //}
                }

            }
            
        } 
    }

    public static function getBidsByProduct($productId)
    {
        return Bid::where('product_id', $productId )->get();
    }

    public static function getBidsByUser($userId)
    {
        return Bid::where('user_id', $userId)->get();
    }

    public static function getAutobids($userId) 
    {
        $userBids = self::getBidsByUser($userId);
        $products = [];
        foreach($userBids as $userBid) {
            $products[$userBid->product_id] = $userBid->auto_bidding;
        }

        return array_keys(array_filter($products));

    }
}
